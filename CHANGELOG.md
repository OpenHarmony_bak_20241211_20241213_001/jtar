## v2.0.1-rc.5
- submodule tar库更新，修复taskpool解压时部分文件丢失的问题

## v2.0.1-rc.4
- submodule tar库更新，修复taskpool解压时部分文件丢失的问题

## v2.0.1-rc.3
- 删除日志打印
- 修复cmake -v报错的问题，改用--log-level=verbose

## v2.0.1-rc.2

- 修复与taskpool结合使用报错的问题
- 
## v2.0.1-rc.1

- 修改非tar工具压缩出来的tar包，解压失败的问题

## v2.0.1-rc.0

- 修复设置自定义解压路径无效的问题
- 优化解压缩接口调用方法

## v2.0.0

- DevEco Studio 版本： 4.1 Canary(4.1.3.317)
- OpenHarmony SDK:API11 (4.1.0.36)
- ArkTs 语法适配
- 将库名称jtar更改为tar
- 以 git submodule 形式依赖c库

## v1.0.2

- 适配DevEco Studio 3.1Beta1版本。
- 适配OpenHarmony SDK API version 9版本。
- 重构工程读写逻辑，不再支持直接命令行操作，采用文件读写操作

## v1.0.1

- api8升级到api9，并转换为stage模型

## v1.0.0

1. 支持tar打包

2. 支持tar解包

